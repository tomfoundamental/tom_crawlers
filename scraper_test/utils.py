
import itertools
import time
from bs4 import BeautifulSoup as BS
from splashutils import Splash
import os
import sys

def splash_request(url,browser):
	# make a get request of a url given a browser object and return the result
	res = browser.get_page(url)
	return res

def curry_splash(url,browser):
	# wrap a splash request of a specific user using a specific browser object in a function
	# to allow us to track the time of the function easily
	def res():
		return splash_request(url,browser)
	return res

def timer(name, meth):
	# time how long it takes to do a function and print the time taken with its name
    t0 = time.time()

    # this is why we need to curry the splash request, above
    res = meth()

    # print how long it took
    display(name + ", took: " + str(time.time() - t0))
    return res

def timed_splash(url, on_load = None, post_load = None):
	# makes a splash request, possibly including some on_load / post_load javascript calls
	# times how long the request took to complete

	# first we create a splash browser
	browser = Splash.Browser.from_config()

	# and set the browser max timeout to the max possible (3600)
	browser.json['timeout'] = 3600

	# then we merge any provided js scripts together using merge_js (below)
	js_source = merge_js(on_load, post_load)

	# if we've provided any scrips, we add these to the browser json to go with the request
	if js_source != None:
		browser.json['js_source'] = js_source

	# then we wrap our splash request as a curried function, for easy timing
	splash = curry_splash(url,browser)

	# then we call it, return the result, and print how long it takes
	return timer("Requested " + url, splash)

def merge_js(on_load = None, post_load = None):
	# merge_js merges two js strings that each may also be none
	# after merging the strings, they're wrapped in a js script that embeds them within
	# a page, and calls them (after it's loaded - eg. for ajax, button clicking, etc.)

	if on_load == None and post_load == None:
		return None
	if post_load == None:
		return on_load
	else:
		# if 
		on_load = page_script(on_load)
		return on_load + post_load

def page_script(page_js):
	# embed a script in a page before calling it.
	return '''
			var pageScript = function() {
				var  PageScript = document.createElement('script')
				PageScript.src = ''' + page_js + '''
				document.head.appendChild(PageScript);
			}
			pageScript()
		'''

