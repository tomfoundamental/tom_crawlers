A scraper is made up of a list of instructions, Python dictionaries of one of the forms below:

find = {
    layer = 'find' : str,
    by = {'class','selector','id','type','attr_has','attr_equals'} : str,
    (equals: str),
    (attr: str),
}

find_all = {
    layer = 'find_all': str,
    by = {'class','id','type','attr_has','attr_equals} : str,
    (equals: str ),
    (attr: str),
}

value = {
    layer = 'value': str,
    of: str
}

text = {
    layer = 'text': str,
    (split: str),
    (on: int),
}

assign = {
    layer = 'assign': str,
    to: str
}

save = {
    layer = 'save': str,
}

loop = {
    layer = 'loop': str,
    prefix: str,
    (suffix: str),
    start: int,
    end: int,
}

splash = {
    layer = 'splash': str,
    url: str,
    (on_load: str),
    (click: str),
}

When run, each instruction takes in the output of the previous instruction, along with its own parameters, and any variables temporarily stored in the scraper object itself (by an Assign call), and return an output for the next instruction (aside from Save and Assign, which return None).

The find_all and loop instructions return a list of outputs. As such, the next instruction after either a find_all or a loop (and the one after that, etc.) is called with each member of the list, in turn. The iteration proceeds depth first.

Instructions are added with the .add() method of the Scraper class, where parameters are provided using keyword arguments (with the 'layer' kwarg defining which type of instruction is to be added).

If the crawler needs to pull out several related datapoints (eg. both the title and url of a div), then this can be accomplished by forking the instruction path.

To do so, one calls .add(fork = [ [ instr: instruction ] ]), passing in a list of lists of instructions, each of which is then executed given the output of the instruction proceeding the fork.

To ease providing such forks, if a Scraper is provided as an instruction to another Scraper, it is replaced with its own list of instructions. As such, one can also define a fork using .add(fork = [ sub_scraper: Scraper ]).

The example provided in this repo has an example of such a fork.


