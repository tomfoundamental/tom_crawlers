
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup as BS
import bs4
from utils import timed_splash

from src.ducks.Wrap import Wrap
from src.ducks.List import List
from src.ducks.Dict import Dict
from src.ducks.Soup import Soup
from src.ducks.DataFrame import DataFrame

class Scraper(Wrap):

	def __init__(self, app, debug):

		self.app = app

		if debug is True:
			self.debug = {
				'changed': True,
				'handler': True
			}
		else:
			self.debug = {}

		self.instructions = []

		self.tests_run = 0

		self.output = []
		self.cache = []

		self.splash_cache = {}

		self.add_updates = [
			self.ran_test_on,

			self.order_test_on,
			
			self.check_new_instruction_on,
			
			self.check_new_command_on,
		]

		Wrap.__init__(self)

	# self

	def ran_test_on(self):
		self.triggers(output = self.ran_test)

	def ran_test(self):
		self.assign(tests_run = self.tests_run + 1)

	# controller

	def check_new_command_on(self):
		self.controller.triggers(new_command = self.check_new_command)

	def check_new_command(self):
		comm = self.controller.get(new_command = None)
		display(comm)
		self.move_up(comm)
		self.move_down(comm)
		self.delete_instruction(comm)

	def move_up(self, comm):
		if comm.get('name','') == 'up':
			indices = comm.get('rows',[])
			self.assign(instructions = List.move_list(self.instructions,indices, -1))

	def move_down(self, comm):
		if comm.get('name','') == 'down':
			indices = comm.get('rows',[])
			self.assign(instructions = List.move_list(self.instructions,indices, 1))

	def delete_instruction(self, comm):
		if comm.get('name','') == 'delete':
			indices = comm.get('rows',[])
			self.assign(instructions = List.not_at(self.instructions,indices))


	# Run instructions

	def order_test_on(self):
		self.controller.triggers(tests_ordered = self.run_test)

	def run_test(self):
		self.output = []
		self.cache = []
		self.run_instruction(0)
		self.assign(output = self.output)


	def run_instruction(self, i):
		if i == len(self.instructions):
			self.output.append(self.cache[-1])
		elif i < len(self.instructions):
			instr = self.instructions[i]
			display('Running instr {}.'.format(instr))
			try:
				if len(self.cache) == 0:
					soup = None
				else:
					soup = self.cache[-1]

				handler = instr.get('handler',lambda soup, instr: soup)
				res = handler(soup, instr)
				
				if type(res) is bs4.element.ResultSet or type(res) is list:
					display('hit list')
					for item in res:
						self.cache.append(item)
						display(type(item))
						self.run_instruction(i + 1)
						del self.cache[-1]
				else:
					self.cache.append(res)
					self.run_instruction(i + 1)
					del self.cache[-1]
			except Exception as e:
				display('Error {} on instr {}'.format(repr(e), instr))
		else:
			pass

	# Check instructions

	def bs(self, soup):
		return BS(soup, 'html5lib')

	def bs_list(self, soups):
		return [ self.bs(str(item)) for item in soups ]

	def check_new_instruction_on(self):
		self.controller.triggers(new_instruction = self.check_new_instruction)

	def check_new_instruction(self):
		instr = self.controller.get(new_instruction = None)
		self.check_load(instr)
		self.check_splash(instr)
		self.check_text(instr)
		self.check_single(instr)
		self.check_many(instr)

	def check_load(self, instr):
		if instr.get('name','') == 'load':
			new_instr = {
				'name': 'load',
				'val': instr['val'],
				'handler': self.load_handler
				}
			self.assign(instructions = self.instructions + [ new_instr ])
	
	def load_handler(self, soup, instr):
		return instr['val']

	def check_splash(self, instr):
		if instr.get('name','') == 'splash':
			new_instr = {
				'name': 'splash_request',
				'handler': self.splash_handler
				}
			self.assign(instructions = self.instructions + [ new_instr ])

	def splash_handler(self, soup, instr):
		cache = self.splash_cache.get(soup, "")
		if cache == "":
			res = self.bs(timed_splash(soup))
			self.splash_cache[soup] = res
		else:
			res = cache
		return res
	
	def check_text(self, instr):
		if instr.get('name','') == 'text':
			new_instr = {
				'name': 'find_text',
				'handler': self.text_handler
				}
			self.assign(instructions = self.instructions + [ new_instr ])

	def text_handler(self, soup, instr):
		return "\n".join(Soup.to_df(soup)['contents'].values.tolist())

	def check_single(self, instr):
		valid_by = ['class', 'id', 'type','attr', 'selector']
		if instr.get('name','') == 'single' and instr.get('features',[])[0] in valid_by:
			new_instr = {
				'name': 'find_single',
				'handler': self.find_single_handler,
				'by': instr.get('features',[])[0],
				'equals': instr.get('vals',[])[0],
				}
			display(new_instr)
			self.assign(instructions = self.instructions + [ new_instr ])

	def find_single_handler(self, soup, instr):
		by = instr.get('by','')
		if by == 'class':
			if isinstance(instr["equals"], list):
				target = " ".join(instr["equals"])
			else:
				target = instr["equals"]
			return self.bs(str(soup.find(class_ = target)))
		elif by == 'selector':
			return self.bs(str(soup.select(instr["equals"])))
		elif by == 'id':
			return self.bs(str(soup.find(id = instr["equals"])))
		elif by == 'type':
			return self.bs(str(soup.find(instr["equals"])))
		elif by == 'attr':
			if instr["method"] == "find_single_by_attr_name":
				return self.bs(str(soup.find(lambda x: x.attrs.get(instr["name"],"") != "")))
			elif instr["method"] == "find_single_by_attr_equals":
				return self.bs(str(soup.find(lambda x: x.attrs.get(instr["name"],"") == instr["equals"])))
		else:
			return soup

	def check_many(self, instr):
		valid_by = ['class', 'id', 'type','attr']
		if instr.get('name','') == 'many' and instr.get('features',[])[0] in valid_by:
			new_instr = {
				'name': 'find_many',
				'handler': self.find_many_handler,
				'by': instr.get('features',[])[0],
				'equals': instr.get('vals',[])[0],
				}
			display(new_instr)
			self.assign(instructions = self.instructions + [ new_instr ])

	def find_many_handler(self, soup, instr):
		by = instr.get('by','')
		if by == 'class':
			if isinstance(instr["equals"], list):
				target = " ".join(instr["equals"])
			else:
				target = instr["equals"]
			display(soup)
			res = soup.find_all(class_ = target)
			return res
		elif by == 'id':
			return soup.find_all(id = instr["equals"])
		elif by == 'type':
			return soup.find_all(instr["equals"])
		elif by == 'attr':
			if instr["method"] == "find_single_by_attr_name":
				return soup.find_all(lambda x: x.attrs.get(instr["name"],"") != "")
			elif instr["method"] == "find_single_by_attr_equals":
				return soup.find_all(lambda x: x.attrs.get(instr["name"],"") == instr["equals"])
		else:
			return soup























