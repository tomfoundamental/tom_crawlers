
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup as BS
import bs4
from trampoline import trampoline
import itertools

from src.ducks.Wrap import Wrap
from src.ducks.List import List
from src.ducks.Dict import Dict
from src.ducks.Soup import Soup
from src.ducks.DataFrame import DataFrame as DF

class Controller(Wrap):

	def __init__(self, app, debug):

		self.app = app

		if debug is True:
			self.debug = {
				'changed': True,
				'handler': True
			}
		else:
			self.debug = {}
		
		self.dom_df = pd.DataFrame()
		self.dom_view_df = pd.DataFrame()
		self.selection_df = pd.DataFrame()
		self.scraper_df = pd.DataFrame()
		self.output_df = pd.DataFrame()

		self.soup_output = {}
		self.soup_view = {}
			
		self.dom_ids = []

		self.selection_features = []
		self.selection_vals = []

		self.tests_ordered = 0

		self.new_instruction = {}
		self.instructions_buffer = []
		self.new_command = {}

		self.contracted = []

		self.add_updates = [
			self.check_load_on,
			self.check_url_on,

			self.dom_rows_on,
			self.selection_rows_on,

			self.check_xpath_on,

			self.expand_all_on,
			self.contract_all_on,
			self.expand_one_on,
			self.contract_one_on,
			self.expand_one_all_on,
			self.contract_one_all_on,

			self.update_dom_view_on,

			self.check_single_on,
			self.check_many_on,
			self.check_text_on,

			self.check_custom_single_on,
			self.check_custom_many_on,

			self.scraper_up_on,
			self.scraper_down_on,
			self.scraper_delete_on,

			self.test_instructions_on,
			self.update_instructions_on,

			self.save_crawler_on,
			self.update_view_on,
		]

		Wrap.__init__(self)

	# updates

	def check_load_on(self):
		self.view.triggers(load_clicks = self.check_load)

	def check_load(self):
		path = self.view.get(header_from = None)
		df = pd.read_csv(path)
		instructions = []
		for i, row in df.itterows():
			instructions.append(row.to_dict())
		self.assign(instructions_buffer = instructions[1:] \
			, new_instruction = instructions[0])


	def check_url_on(self):
		self.view.triggers(url_clicks = self.check_url)

	def check_url(self):
		url = self.view.get(header_url = None)
		self.assign(instructions_buffer = [ 
			{
				'name': 'splash'
			}
		])
		self.assign(new_instruction = {
				'name': 'load',
				'val': url
			})


	def dom_rows_on(self):
		self.view.triggers(dom_rows = self.dom_rows)

	def dom_rows(self):
		dom_rows = self.view.get(dom_rows = None)
		display(dom_rows)
		if len(dom_rows) > 0:
			display('dom len > 0')
			view_df = self.dom_view_df
			dom_df = self.dom_df

			row_df_id = view_df.iloc[dom_rows[0]]['df_id']
			ids_selected = [ row_df_id ]

			selected_row = dom_df[dom_df['df_id'] == ids_selected[0]]
			
			non_empty = [ col for col in selected_row.columns if selected_row.iloc[0][col] != '' ]
			selected_row = selected_row[non_empty]

			new_selection = np.transpose(selected_row)
			new_selection.columns = ['selected']

			display(new_selection)
			self.assign(selection_df = new_selection, dom_ids = ids_selected)


	def selection_rows_on(self):
		self.view.triggers(selection_rows = self.selection_rows)

	def selection_rows(self):
		selection_rows = self.view.get(selection_rows = None)
		if len(selection_rows) > 0:
			display('selection len > 0')
			selection_df = self.selection_df

			features = DF.index_as_list(selection_df)

			features_selected = [ item for item, i in zip(features, range(len(features))) if i in selection_rows ]

			num_features = len(features_selected)

			selected = selection_df.loc[features_selected]
			selected_vals = selected['selected'].values.tolist()
			
			display(selected_vals)
			display(features_selected)

			self.assign(selection_vals = selected_vals, selection_features = features_selected)



	def get_id(self, node):
		return node['df_id']

	def bounce_init(self, node):
		return iter([node])

	def bounce_step(self, acc, node, parser):
		children_generator = yield self.bounce(node, parser)
		return itertools.chain(acc, children_generator)

	def bounce_check(self, child):
		if child['df_id'] not in self.contracted:
			return True
		return False

	def add_smallest(self, new, children):
		smallest = 10000000000000
		loc = None
		for i, child in enumerate(children,0):
			df_id = int(child['df_id'])
			if df_id < smallest:
				smallest = df_id
				loc = i
		return new + [ children[loc] ]

	def sort_chilren(self, children):
		new = []
		for child in children:
			new = self.add_smallest(new, children)
		return new


	def bounce(self, node, parser):
		try:
			parsed_node = parser(node)

			acc = self.bounce_init(parsed_node)

			children = node.get('children',[])
			children = self.sort_chilren(children)

			if len(children) > 0 and self.bounce_check(node):
				for child in children:
					acc = yield self.bounce_step(acc, child, parser)
			return acc
		except Exception as e:
			display(e)
			return acc

	def remove_children(self, node):
		new = {}
		for key, val in node.items():
			if key != 'children':
				new[key] = val
		return new

	def dict_tree_to_df(self, tree):
		result = trampoline(self.bounce(tree, self.remove_children))
		result = list(result)
		return self.to_df(result)

	# remove the hidden column afterwards

	def get_descendants(self, node, df_id, results, found_id = False, max_depth = 1, depth = 0):
		if max_depth is not None:
			if depth == max_depth:
				return acc
		if node['df_id'] == df_id:
			found_id = True
		children = node.children
		for child in children:
			results = yield self.get_descendants(child, df_id, results, found_id = found_id, max_depth = max_depth, depth = depth)
		return results

	def get_descendants_list(self, ids):
		res = iter([])
		for each_id in ids:
			descendants = self.get_descendants(self.soup_output, each_id, [], max_depth = None)
			res = itertools.chain(res, descendants)
		return res


	def update_dom_view_on(self):
		self.triggers(contracted = self.update_dom_view)

	def update_dom_view(self):
		df = self.dict_tree_to_df(self.soup_output)
		display(df)
		self.assign(dom_view_df = df)


	def expand_all_on(self):
		self.view.triggers(dom_expand_all_clicks = self.expand_all)

	def expand_all(self):
		self.assign(contracted = [])


	def contract_all_on(self):
		self.view.triggers(dom_contract_all_clicks = self.contract_all)

	def contract_all(self):
		self.assign(contracted = ['1'])


	def expand_one_on(self):
		self.view.triggers(dom_expand_one_clicks = self.expand_one)

	def expand_one(self):
		pass


	def contract_one_on(self):
		self.view.triggers(dom_contract_one_clicks = self.contract_one)

	def contract_one(self):
		pass


	def expand_one_all_on(self):
		self.view.triggers(dom_expand_one_all_clicks = self.expand_one_all)

	def expand_one_all(self):
		self.assign(contracted = [item for item in self.contracted \
			if item not in self.dom_ids \
			and item not in self.get_descendants_list(self.dom_ids)])


	def contract_one_all_on(self):
		self.view.triggers(dom_contract_one_all_clicks = self.contract_one_all)

	def contract_one_all(self):
		self.assign(contracted = self.contracted + self.dom_ids)
	

	def check_text_on(self):
		self.view.triggers(select_text_clicks = self.check_text)

	def check_text(self):
		self.assign(new_instruction = {
			'name': 'text',
		})

	def check_xpath_on(self):
		self.view.triggers(xpath_clicks = self.check_xpath)

	def check_xpath(self):
		self.assign(new_instruction = {
			'name': 'single',
			'features': ['selector'],
			'vals': [ self.view.get(xpath = None )],
		})

	def get_custom_instruction(self):
		_type = self.view.get(selected_type = '')
		_id = self.view.get(selected_id = '')
		_class = self.view.get(selected_class = '')
		if _type != '':
			return { 'features': ['type'], 'vals': [_type] }
		elif _id != '':
			return { 'features': ['id'], 'vals': [_id] }
		elif _class != '':
			return { 'features': ['class'], 'vals': [_class] }
		else:
			return None

	def check_custom_single_on(self):
		self.view.triggers(custom_single_clicks = self.check_custom_single)

	def check_custom_single(self):
		instr = self.get_custom_instruction()
		if instr is not None:
			instr['name'] = 'single'
			self.assign(new_instruction = instr)


	def check_custom_many_on(self):
		self.view.triggers(custom_many_clicks = self.check_custom_many)

	def check_custom_many(self):
		instr = self.get_custom_instruction()
		if instr is not None:
			instr['name'] = 'many'
			self.assign(new_instruction = instr)


	def check_many_on(self):
		self.view.triggers(select_many_clicks = self.check_many)

	def check_many(self):
		self.assign(new_instruction = {
			'name': 'many',
			'features': self.selection_features,
			'vals': self.selection_vals,
		})


	def check_single_on(self):
		self.view.triggers(select_one_clicks = self.check_single)

	def check_single(self):
		self.assign(new_instruction = {
			'name': 'single',
			'features': self.selection_features,
			'vals': self.selection_vals,
		})


	def scraper_up_on(self):
		self.view.triggers(scraper_up_clicks = self.scraper_up)
	
	def scraper_up(self):
		self.assign(new_command = {
			'name': 'up',
			'rows': self.view.get(scraper_rows = None)
		})


	def scraper_down_on(self):
		self.view.triggers(scraper_down_clicks = self.scraper_down)

	def scraper_down(self):
		self.assign(new_command = {
			'name': 'down',
			'rows': self.view.get(scraper_rows = None)
		})


	def scraper_delete_on(self):
		self.view.triggers(scraper_delete_clicks = self.scraper_delete)

	def scraper_delete(self):
		self.assign(new_command = {
			'name': 'delete',
			'rows': self.view.get(scraper_rows = None)
		})
		
	
	def test_instructions_on(self):
		self.view.triggers(refresh_clicks = self.test_instructions)

	def test_instructions(self):
		self.assign(tests_ordered = self.tests_ordered + 1)


	def save_crawler_on(self):
		self.view.triggers(save_clicks = self.save_crawler)

	def save_crawler(self):
		to = self.view.get(header_to = None)
		instructions = self.scraper.get(instructions = None)
		instructions_df = self.to_df(instructions)
		instructions_df.to_csv(to)


	def update_instructions_on(self):
		self.scraper.triggers(instructions = self.update_instructions)

	def update_instructions(self):
		if len(self.instructions_buffer) > 0:
			new_instruction = self.instructions_buffer[0]
			self.assign(instructions_buffer = self.instructions_buffer[1:])
			self.assign(new_instruction = new_instruction)
		df = self.to_df(self.scraper.get(instructions = None))
		self.assign(scraper_df = df)


	def update_view_on(self):
		self.scraper.triggers(tests_run = self.update_view)

	def to_row(self, item, i):
		fresh = {}
		for key, val in item.items():
			if hasattr(val, '__call__'):
				fresh[key] = val.__name__
			else:
				fresh[key] = val
		row = pd.Series(fresh)
		row.name = i
		return row

	def to_df(self, dict_list):
		df = pd.DataFrame([ self.to_row(item, i) for item, i in zip(dict_list,range(len(dict_list)))])
		df.fillna('')
		return df

	def update_view(self):
		result = self.scraper.get(output = None)

		if type(result[0]) is bs4.BeautifulSoup:
			output = Soup.to_dict(result[0])
			soup_df = Soup.to_df(result[0])

			self.assign(soup_output = output, dom_df = soup_df, dom_view_df = soup_df)
			self.assign(output_df = pd.DataFrame(), selection_df = pd.DataFrame())
		else:
			display(result)
			output_df = self.to_df(result)
			
			self.assign(soup_output = BeautifulSoup(), dom_df = pd.DataFrame(), dom_view_df = pd.DataFrame())
			self.assign(output_df = output_df, selection_df = pd.DataFrame())
		




























