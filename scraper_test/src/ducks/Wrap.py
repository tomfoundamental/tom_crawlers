

class Wrap():

	def __init__(self):
		self.__listeners__ = {}

	def get(self, **kwargs):
		res = []
		for key, val in kwargs.items():
			if hasattr(self, key):
				to_add = getattr(self, key)
				if hasattr(to_add,'copy'):
					res.append(to_add.copy())
				else:
					res.append(to_add)
					if type(to_add) is not int and type(to_add) is not str \
					and type(to_add) is not float and type(to_add) is not bool:
						display('Warning - risky to not copy object {} from {}.'.format(to_add,type(self)))
			else:
				display('Attempted to get non-existent key {} of object type {}.'.format(key,type(self)))
				res.append(val)
		if len(res) == 1:
			return res[0]
		return tuple(res)

	def __if_has__(self, key, handler):
		if hasattr(self,key):
			handler(getattr(self,key))

	def __my_debug__(self, debug, **kwargs):
		for key, val in kwargs.items():
			if key in debug:
				if debug[key] is True:
					self.__debug_print__(key, val)

	def __debug_print__(self, key, val):
		if key == 'changed':
			display('{} changed, calling listening handlers.'.format(val))
		elif key == 'handler':
			display('Change handler {} called.'.format(val))
		else:
			display('Tried to debug key {} with val {}.'.format(key, val))


	def __add_updates__(self):
		for add_update in self.add_updates:
			add_update()

	def __add_trigger__(self, key, handler):
		if key in self.__listeners__:
			self.__listeners__[key].append(handler)
		else:
			self.__listeners__[key] = [handler]

	def __get_handlers__(self, key):
		return self.__listeners__.get(key, [])

	def __on_change__(self, key):
		handlers = self.__get_handlers__(key)
		self.__if_has__('debug', lambda debug: self.__my_debug__(debug, changed = key))
		for handler in handlers:
			self.__if_has__('debug', lambda debug: self.__my_debug__(debug, handler = handler))
			handler()

	def triggers(self, **kwargs):
		for key, handler in kwargs.items():
			if hasattr(self, key):
				self.__add_trigger__(key, handler)
			else:
				display('Warning - trying to listen to {} which does not exist for class {}.'.format(key, type(self)))

	def assign(self, **kwargs):
		for key, val in kwargs.items():
			if hasattr(self, key):
				setattr(self, key, val)
				self.__on_change__(key)
			else:
				display('Warning - creating new attribute of {} on object with class {}.'.format(key, type(self)))

