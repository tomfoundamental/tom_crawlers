
from src.ducks.type_error import type_error

class Duck:
	def __init__(self):
		self.listeners = []

	def throw(self):
		raise type_error('Duck','anything')

	def triggers(self, listener):
		self.listeners.append(listener)

	def on_change(self):
		for listener in self.listeners:
			listener()

	def to(self, val):
		if self.valid(val):
			new = type(self)(val)
			new.listeners = self.listeners
			self = new
		else:
			self.throw()
		return self

	def to_if(self, cond, val, else_val = None):
		if cond == True:
			return self.to(val)
		else:
			if else_val is not None:
				return self.to(else_val)
			else:
				return self



