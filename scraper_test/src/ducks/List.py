
class List:
	def __init__(self):
		pass

	@classmethod
	def length(cls, lis):
		return len(lis)

	@classmethod
	def last_index(cls, lis):
		return cls.length(lis) - 1

	@classmethod
	def longer_than(cls, lis, val):
		return cls.length(lis) > val

	@classmethod
	def lt(cls, lis, val):
		return cls.longer_than(lis,val)

	@classmethod
	def shorter_than(cls, lis, val):
		return cls.length(lis) < val

	@classmethod
	def st(cls, lis, val):
		return cls.shorter_than(lis,val)


	@classmethod
	def move_down(cls, lis, what):
		what = int(what)
		last = cls.last_index(lis)

		if what < 0:
			return lis
		if what == 0:
			return lis
		else:
			return lis[0: what - 1] + [ lis[what] ] + [lis[what - 1]] + lis[what + 1:]

	@classmethod
	def move_up(cls, lis, what):
		what = int(what)
		last = cls.last_index(lis)

		if what > last:
			return lis
		elif what == last:
			return lis
		else:
			return lis[0: what ] + [ lis[what + 1 ]] + [ lis[what] ] + lis[what + 2:]

	@classmethod
	def move(cls, lis, what, by):
		what = int(what)
		last = cls.last_index(lis)
		new_index = max(min(what + by, last),0)

		if by > 0:
			for i in range(by):
				lis = cls.move_up(lis, what)
				what = what + 1
		else:
			for i in range(-by):
				lis = cls.move_down(lis, what)
				what = what - 1
		return lis

	@classmethod
	def move_list(cls, lis, what, by):
		what = [ int(item) for item in what ]
		what.sort()
		if by < 0:
			what.reverse()

		for index in what:
			res = cls.move(lis, index, by)
		return res


	@classmethod
	def at(cls, lis, locs):
		locs = [ int(item) for item in locs ]
		return [ item for item, i in zip(lis, range(cls.length(lis))) if i in locs ]

	@classmethod
	def not_at(cls, lis, locs):
		locs = [ int(item) for item in locs ]
		display(lis, locs)
		return [ item for item, i in zip(lis, range(cls.length(lis))) if i not in locs ]


	@classmethod
	def where_in(cls, lis, other):
		return [item for item in lis if item in other]

	@classmethod
	def where_not_in(cls, lis, other):
		return [item for item in lis if item not in other]

	@classmethod
	def where(cls, lis, pred):
		return [item for item in lis if pred(item)]

	@classmethod
	def where_not(cls, lis, pred):
		return [item for item in lis if not pred(item)]


	@classmethod
	def loop(cls, lis, handler):
		for item in lis:
			# fun check?
			handler(item)

	@classmethod
	def enum(cls, lis, handler):
		for i, item in enumerate(lis, 0):
			# fun check?
			handler(i, item)

	@classmethod
	def reduce(cls, lis, handler, accumulator):
		for item in lis:
			# fun check?
			accumulator = handler(item, accumulator)
		return accumulator