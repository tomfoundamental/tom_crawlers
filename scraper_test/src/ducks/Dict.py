

class Dict:
	def __init__(self):
		pass

	@classmethod
	def get_into_get(cls, a, key, b):
		val = a.get(key,'')
		return b.get(val,'')

	@classmethod
	def get_into_get_handler(cls, a, key, handler_switch):
		handler = cls.get_into_get(a, key, handler_switch)
		if handler == '':
			display("Key val switch - no handler for key {} with value {}".format(key, a.get(key,'')))
		else:
			return handler()