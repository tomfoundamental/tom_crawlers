
import itertools
import pandas as pd
import numpy as np
from trampoline import trampoline
import bs4

class Soup:
	_id = 0

	def __init__(self):
		pass
	
	@classmethod
	def find_parent(cls, of, nodes):
		for i, node in enumerate(nodes,0):
			if node['df_id'] == of['parent']:
				return i

	@classmethod
	def to_dict(cls, soup, list_index = 0):
		display('Called to dict')
		results = list(cls.to_dict_list(soup))
		results.reverse()
		for i, node in enumerate(results,0):
			if node['df_id'] != '1':
				parent_loc = cls.find_parent(node, results)
				parent = results[parent_loc]
				if parent.get('children','') == '':
					parent['children'] = [] 
				parent['children'].append(node)
		return results[-1]
		

	@classmethod
	def node_to_row(cls,node,i):
		row = pd.Series(node)
		row.name = str(i)
		return row

	@classmethod
	def to_df(cls, soup, list_index = 0):
		results = list(cls.to_dict_list(soup))
		df = pd.DataFrame([cls.node_to_row(row,i) for row, i in zip(results, range(len(results))) ])
		df = df.fillna('')
		return df

	@classmethod
	def to_dict_list(cls, soup, list_index = 0):
		cls._id = 1
		return trampoline(cls.list_bounce_down(soup, cls.to_dict_list_handler, list_index = list_index))

	@classmethod
	def to_dict_list_handler(cls, data, list_index, _parent, _depth):
		node = {
				'source_index': str(list_index),
				'parent': str(_parent),
				'df_id': str(cls._id),
				'type': data['name'],
				'prefix': data['prefix'],
				'sibling_depth': str(_depth)
			}
		for key, val in data['attrs'].items():
			node[key] = val
		return node

	@classmethod
	def list_bounce_down(cls, div, handler, _parent = 0, _depth = 0, list_index = 0):

		acc_init = lambda node: iter([node])

		def acc_step(acc, child, _parent, child_depth):
			children_generator = yield cls.list_bounce_down(child, handler, _parent = _parent, _depth = child_depth, list_index = list_index, )
			return itertools.chain(acc, children_generator)

		acc = cls.bounce_down(div, handler, acc_init, acc_step, _parent = _parent, _depth = _depth, list_index = list_index, )

		return acc

	@classmethod
	def bounce_down(cls, div, handler, acc_init, acc_step, _parent = 0, _depth = 0, list_index = 0):
		try:
			data = div.__dict__
			node = handler(data, list_index, _parent, _depth)

			_parent = cls._id
			children = div.find_all(recursive = False)

			acc = acc_init(node)
			child_depth = 0
			if len(children) > 0:
				for child in children:
					cls._id = cls._id + 1
					acc = yield acc_step(acc, child, _parent, child_depth)
					child_depth = child_depth + 1
			return acc
		except Exception as e:
			display(e)
			return acc






















