
import pandas as pd
import numpy as nps

class DataFrame:
	def __init__(self):
		pass

	@classmethod
	def length(cls, df):
		return len(df.index)

	@classmethod
	def width(cls, df):
		return len(df.columns)

	@classmethod
	def longer_than(cls, df, val):
		return cls.length(df) > val

	@classmethod
	def lt(cls, df, val):
		return cls.longer_than(df,val)

	@classmethod
	def shorter_than(cls, df, val):
		return cls.length(df) < val

	@classmethod
	def st(cls, df, val):
		return cls.shorter_than(df,val)

	@classmethod
	def wider_than(cls, df, val):
		return cls.width(df) > val

	@classmethod
	def wt(cls, df, val):
		return cls.wider_than(df,val)

	@classmethod
	def thinner_than(cls, df, val):
		return cls.width(df) < val

	@classmethod
	def tt(cls, df, val):
		return cls.thinner_than(df,val)

	@classmethod
	def row_to_list(cls, df, row):
		return df.loc[row].values.tolist()

	@classmethod
	def col_to_list(cls, df, col):
		return df[col].values.tolist()

	@classmethod
	def index_as_list(cls, df):
		return df.index.tolist()

	@classmethod
	def rows_where_col_is(cls, df, col, pred):
		return df[pred(df[col])]

	@classmethod
	def rows_where_col_is_not(cls, df, col, pred):
		return df[~pred(df[col])]

	@classmethod
	def rows_where_col_equals(cls, df, col, val):
		return df[df[col] == val]

	@classmethod
	def rows_where_col_equals_not(cls, df, col, val):
		return df[~df[col] == val]

	@classmethod
	def rows_where_col_in(cls, df, col, vals):
		return df[df[col].isin(vals)]

	@classmethod
	def rows_where_col_not_in(cls, df, col, vals):
		return df[~df[col].isin(vals)]

	@classmethod
	def col_names_where_row_equals(cls, df, row, val):
		display('not done yet')
		pass

	@classmethod
	def cols_where_row_equals(cls, df, row, val):
		# unsure?
		return df.loc[val]

	@classmethod
	def insert_row_at(cls, df, row, iloc):
		res = pd.DataFrame(np.vstack())
		# set columns
		df.to(res)
		return res

	@classmethod
	def first_row(cls, df):
		return df.iloc[0]

	@classmethod
	def last_row(cls, df):
		return df.iloc[-1]









