
from src.View import View
from src.Controller import Controller
from src.Scraper import Scraper

class App:

	def __init__(self, debug = False):

		scraper = Scraper(self, debug = debug)
		controller = Controller(self, debug = debug)
		view = View(self, debug = debug)

		add_components(self, scraper = scraper, controller = controller, view = view)
		add_components(scraper, controller = controller, view = view)
		add_components(controller, scraper = scraper, view = view)
		add_components(view, scraper = scraper, controller = controller)

		scraper.__add_updates__()
		view.__add_updates__()
		controller.__add_updates__()


def add_components(obj, scraper = None, controller = None, view = None):

	if scraper is not None:
		obj.scraper = scraper

	if controller is not None:
		obj.controller = controller

	if view is not None:
		obj.view = view
