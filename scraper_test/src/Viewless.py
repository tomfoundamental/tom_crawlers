
import numpy as np
import pandas as pd
from IPython.display import display
from bs4 import BeautifulSoup as BS
import bs4
from utils import timed_splash


class Scraper:
    def __init__(self, root = '', current = None, path = None, results_path = None):
        # root is the root url for the scraper
        self.root = root

        # current is a BS object to call the next (ie. here, first) scraper instruction on
        # current is then also used to store the result of the previous instruction call
        self.current = current

        # path is the location of a csv of instructions, to init the scraper with
        self.path = path
        
        # results_path is a location at which to save the scraper output
        self.results_path = results_path

        # instructions is a container for the instructions that make up the scraper
        self.instructions = []

        # data holds the set of values that have currently been asigned to keys
        self.data = {}

        # results holds the final list of data objects that have been saved by the scraper
        self.results = []

        # cache is used to hold temporary results in memory between instruction calls
        self.cache = []

        # current_instr holds the instruction currently being ran
        self.current_instr = {}

        # if a path is provided, load the scraper with the instructions at that path
        if path is not None:
            df = pd.read_csv(path)
            self.instructions = df.to_dict('records')
        
    # SECTION 1
    # add new instructions
        
    def add(self, **kwargs):
        # used to add a scraper layer / fork to our current scraper
        # passing in the relevant layer parameters as kwargs

        # layer kwarg refers to the type of instruction being added
        # 'layer' is used instead of 'instruction' because a list of instructions
        # or a whole scraper can also be passed as a scraper layer
        layer = kwargs.get('layer','')

        # forks are defined using a separate kwarg than instructions as they are 
        # parsed somewhat differently here
        fork = kwargs.get('fork','')

        # if both are provided, will default to parsing the layer part of kwargs
        if layer != '':
            # first, we check if the kwargs is a valid instruction
            if self.check_instruction(kwargs):

                # if the layer is a Scraper, we unpack the scrapers instructions
                # as a list, and store them as a list at the end of self.instructions
                if type(layer) is Scraper:
                    self.instructions.append(layer.instructions)

                # else, we append the layer to our list of instrctions as is
                else:
                    self.instructions.append(kwargs)

            # if the kwargs is not a valid instruction and a fork has not been provided
            # we will fall through to the error case at the end of this function
        elif fork != '':
            # first, check the fork provided is a list (of branches to fork down)
            if type(fork) is not list:
                display('Error - must provide a list of layers when using fork kwarg.')
            else:
                # init a validity accumulator
                valid = True
                # check the validity of each strand of the fork
                for instr in fork:
                    valid = valid and self.check_instruction(instr)

                # if the fork is valid, unpack each branch and save in self.instructions
                if valid == True:
                    self.instructions.append([ self.unpack(instr) for instr in fork ])
        else:
            display('Error - must provide either a layer or a fork kwarg parameter.')

    def unpack(self, scraper):
        # if the layer is a scraper, return the instructions contained within as a list

        if type(scraper) is Scraper:
            return scraper.instructions
        # if the layer is not a scraper, simply return the instructions
        return scraper
    
    def instruction(self, **kwargs):
        # legacy function for translating the add() kwargs into an instruction dict

        if self.check_instruction(kwargs) is True:
            # as it stands, just returns the kwargs back
            return kwargs
    
    def check_instruction(self, kwargs):
        # checks an instruction (normally provided as kwargs)
        # returns true if valid, false if not

        # if the instruction to be checked is a scraper, by definition valid, return true
        if type(kwargs) is Scraper:
            return True

        # otherwise, take the layer param from the instruction
        layer = kwargs.get('layer','')

        # if the layer is a scraper, again, by definition valid
        if type(layer) is Scraper:
            return True

        # else, check the layer name itself is valid, return true
        if self.valid_layer(layer):

            # if the name is valid, check if the params are valid given the name
            if self.valid_instruction(layer, kwargs):
                # if so, instr is valid, return true
                return True

        # if we fall to here, instruction not valid, return false
        return False
            
    def valid_layer(self, layer):
        # checks if a layer string is valid
        # if it is, return true, else return false

        # list of valid instructions:
        valid = [ 'scraper', 'splash','find','find_all','text','value','assign', 'save', 'loop' ]

        # return true if in valid, else return false
        if layer in valid:
            return True
        else:
            display('Error - attempting to add layer {}, layer not found.'.format(layer))
            return False
    
    def valid_instruction(self, layer, instr):
        # switch statement that calls the relevant instruction checker given a layer name
        # and the instruction
        # returns true if valid, false if not

        if layer == 'scraper':
            return self.check_scraper(instr)
        elif layer == 'loop':
            return self.check_loop(instr)
        elif layer == 'splash':
            return self.check_splash(instr)
        elif layer == 'find':
            return self.check_find(instr)
        elif layer == 'find_all':
            return self.check_find_all(instr)
        elif layer == 'text':
            return self.check_text(instr)
        elif layer == 'value':
            return self.check_value(instr)
        elif layer == 'assign':
            return self.check_assign(instr)
        elif layer == 'save':
            return self.check_save(instr)
        else:
            display('Error - checking layer {}, not found a case.'.format(layer))
            return False

    # SECTION 2
    # instruction checkers / runners

    def param(self, param, parse = True):
        # parameter unpacker, uses instruction[param] to get a key
        # if param is true:
            # if the key is in the form data[sub_key], returns self.data[sub_key]
            # else, returns instruction.get(key)
        # else:
            # returns instruction[param]
        # if param not found, return None

        if param in self.current_instr:
            # get the key out of the instruction (as param)
            param = self.current_instr[param]

            # parse the param (in case it's self referential)
            if parse == True and type(param) is str:

                # find square brackets to decide if self referential
                open_sq = param.find('[')
                close_sq = param.find(']')

                # if square brackets found, parse self reference param
                if open_sq != -1:

                    # split around the square barckets
                    pre_sq = param[:open_sq]
                    bet_sq = param[open_sq + 1:close_sq]

                    # if the square brackets are preceded by 'data', get from self.data
                    if pre_sq == 'data':
                        return self.data.get(bet_sq, None)

                    # else, invalid format, return None
                    else:
                        return None
            # parse set to False, just return the param from self.instruction
            return param

        # param not found in instruction, return None
        return None
    
    def check_scraper(self, instr):
        # check a scraper instruction for if its of type scraper
        # if it is, return true, else return false

        if type(instr) is Scraper:
            return True
        else:
            display('Error - attempting to add a scraper layer with an obj that is not a scraper.')
            return False
    
    def scraper(self):
        # call a scraper instruction
        # theoretically should never happen as scrapers should be unpacked into a list
        # of instructions when they're added as a layer
        # hence, return None (which triggers an error) and call out the error.

        display('Calling a scraper - should not be possible!')
        return None
    
    def check_find(self, instr):
        # check a find instruction for a valid by and that it has equals / attr
        # eg. by class, id, etc.
        # return true if valid, false if not

        # list of valid find by values
        valid_by = ['class','selector','id','type','attr_has','attr_equals']

        # if not valid, call error and return false
        if instr.get('by','') not in valid_by:
            display('Error - must provide a by for a find single.')
            return False

        # instr must have either / or an equals or attr parameter
        if instr.get('equals','') == '':

            # if no equals, check for attr
            if instr.get('attr','') == '':

                # call error and return false if not found
                display('Error - must provide an equals, unless you provide an attr, for find single.')
                return False
            else:
                # found attr, valid find, return true
                return True
        # found equals, valid find, return true
        return True
    
    def find(self):
        # call a find instruction
        # returns the result of the find instruction called on the previous layer's output
        # which is normally a beautiful soup object, but sometimes a div attribute


        # get find instruction params, none if not present in instruction
        by = self.param('by')
        equals = self.param('equals')
        attr = self.param('attr')

        if by == 'class':
            return self.current.find(class_ = equals)
        elif by == 'selector':
            return self.current.select(equals)
        elif by == 'id':
            return self.current.find(id = equals)
        elif by == 'type':
            return self.current.find(equals)
        elif attr is not None:
            if by == 'attr_has':
                return self.current.find(lambda x: x.attrs.get(attr,"") != "")
            elif iby == "attr_equals":
                return self.current.find(lambda x: x.attrs.get(attr,"") == equals)
        else:
            display('Error - find single by an unrecognised method')
            return None
        
    def check_find_all(self, instr):
        valid_by = ['class','id','type','attr_has','attr_equals']
        if instr.get('by','') not in valid_by:
            display('Error - must provide a by for a find all.')
            return False
        if instr.get('equals','') == '':
            if instr.get('attr','') == '':
                display('Error - must provide an equals, unless you provide an attr, for find all.')
                return False
            else:
                return True
        return True
    
    def find_all(self):
        # call a find_all instruction
        # returns the result of the find instruction called on the previous layer's output
        # normally a beautiful soup list object (type ResultSet)


        # get find_all instruction params, none if not present in instruction
        by = self.param('by')
        equals = self.param('equals')
        attr = self.param('attr')

        if by == 'class':
            return self.current.find_all(class_ = equals)
        elif by == 'id':
            return self.current.find_all(id = equals)
        elif by == 'type':
            return self.current.find_all(equals)
        elif attr is not None:
            if by == 'attr_has':
                return self.current.find_all(lambda x: x.attrs.get(attr,"") != "")
            elif iby == "attr_equals":
                return self.current.find_all(lambda x: x.attrs.get(attr,"") == equals)
        else:
            display('Error - find many by an unrecognised method')
            return None
        
    def check_text(self, instr):
        # checks a text instruction
        # simply returns true as, given its a text instructions, 
        # it has no other mandatory features

        return True
    
    def text(self):
        # calls a text instruction
        # gets the text out of a BS object, possibly splitting it
        # and returning an element of the post-split list

        # get the string to split on (None if no split)
        split_on = self.param('split')
        # get the index of the post-split list item to return (None if no split)
        on = self.param('on')
        
        # get text from the current node and replace any new lines / tabs / spare whitespace
        val = self.current.text
        val = val.replace('\n','')
        val = val.replace('\t','')
        val = val.strip()

        # if both split_on and on (the index to return) are not None
        if split_on is not None and on is not None:
            
            # split the text
            split = val.split(on)

            # if the result is a list (and so find by index is valid)
            if type(split) is list:

                # check if split index is actually within the post-split index range
                if split_on < len(split):

                    # return the selected item
                    return val.split(on)[split_on]

        # default to returning the cleaned text from the previous node
        return val
    
    def check_value(self, instr):
        # checks a value instruction (gets the value of a certain parameter)
        # if the key to get ('of') is provided, returns true, else returns false

        if instr.get('of','') == '':
            display('Error - must provide an of for value call.')
            return False
        return True
    
    def value(self):
        # calls a value instruction (ie. gets the value of an attribute of a BS object)

        of = self.param('of')
        return self.current.attrs.get(of,"")
    
    def check_assign(self, instr):
        # checks an assign instruction (assigns the result of the previous layer
        # to a key in self.data )
        # if the 'to' parameter (key to assign to) is provided, return true, else false

        if instr.get('to','') == '':
            display('Error - must provide a to for an assign call.')
            return False
        return True
    
    def assign(self):
        # calls an assign instruction, saving the result of the previous layer to
        # self.data with to as the param

        # the key to save the result of the previous layer to
        to = self.param('to')
        self.data[to] = self.current

        # returns True, as each instruction has to return (None left to trigger errors)
        return True
    
    def check_save(self, instr):
        # checks a save instruction
        # simply returns true as, given its a save instructions, 
        # it has no features (to be valid or not)

        return True
    
    def save(self):
        # calls a save instruction - saves the current value of self.data to self.results

        self.results.append(self.data.copy())
        self.data = {}

        # returns True, as each instruction has to return (None left to trigger errors)
        return True
    
    def check_splash(self, instr):
        # checks a splash instruction (only mandatory feature is a url to get)
        # returns true if a url is provided, false otherwise

        if instr.get('url','') == '':
            display('Error - must provide a url for a splash request.')
            return False
        return True
    
    def splash(self):
        # calls a splash instruction, possibly also calling a js script on load (eg. for
        # ajax based pages ), and optionally clicking a button - targetted with a 
        # css selector (not xpath) - until the number of divs on the page doesn't change 
        # post-click.
        # returns a BS tree, or None if an error.

        # the url to request
        url = self.param('url')

        # any js you need to call post-load (eg. for ajax)
        on_load = self.param('on_load', parse = False)

        # the css selector of any buttons to (repeatedly) click
        click = self.param('click', parse = False)
        
        # if a css selector is provided in click, wrap it into a javascript string
        # using click_js
        if click is not None:
            post_load = self.click_js(click)
        else:
            post_load = None

        # if the splash is defined as a relative request, then assume the url is a suffix to
        # the root url.
        relative = self.param('relative')
        if relative is True:
            # clean off un-needed slashes
            if self.root[-1] == '/' and url[0] == '/':
                url = url[1:]
            # set the url to the root plus the suffix defined in url
            url = self.root + url

        # if no error occurred, call timed splash with the url and javascripts (where provided)
        # and return the result wrapped into a BS object
        if url is not None:
            return BS(timed_splash(url, on_load = on_load, post_load = post_load), 'html5lib')
        else:
            display('Error - url for splash request was none.')
            return None

    def click_js(self, selector):
        # wraps a css selector into a javascript snippet, that will click the button 
        # every second until the number of divs on the page stops changing
        return '''
        function clickyClick() {
            setTimeout(function() {
                document.querySelector("''' + selector + '''").click();
            }, 1000);
            return document.getElementsByTagName("div").length;
        }
        function loopyClick() {
            var preClickDivs = document.getElementsByTagName("div").length;
            var postClickDivs = clickyClick();
            while (preClickDivs != postClickDivs) {
                postClickDivs = clickyClick();
            }
        }
        loopyClick()
        '''
    
    def check_loop(self, instr):
        # checks a loop instruction, valid if a prefix and start parameter are provided
        # if so, return true, else return false

        if instr.get('prefix','') == '':
            display('Error - must provide a prefix for loop.')
            return False
        if type(instr.get('start','')) is not int:
            display('Error - must provide an int type start.')
            return False
        return True
    
    def loop(self):
        # calls a loop instruction
        # returns a list of strings defined by inserting an iterand from start to end 
        # between prefix and suffix.
        # As it returns a list, the next instruction (and those after) will be called
        # on each item of the resulting list

        # the start of the string to iterate (eg. url stem)
        prefix = self.param('prefix')

        # an optional end of the string to iterate
        suffix = self.param('suffix')

        # numeric index to start the iteration at
        start = self.param('start')

        # optional index to end the iteration at
        # if no end is provided, defaults to 50 (ie. 50 ish pages)
        end = self.param('end')
        
        if end is None:
            # if end is none, set to 50
            # as the instruction has to return an explicit output (didn't get so far
            # as to writing it to take generators)
            end = 50
        indices = range(start,end)
        
        return [ prefix + str(i) + suffix for i in indices ]

    # SECTION 3
    # run instructions

    def run(self, current = None):
        # runs the current set of instructions
        # optionally given a starting value (in current)

        # first, reset the scraper results
        self.results = []

        # and reset the cache
        self.cache = []

        # and the current data
        self.data = {}

        # and the current instruction
        self.current_instr = {}

        # then, if a starting value is provided, assign to self.current
        self.current = current

        try:
            # then, try to step (call the next stored instruction)
            # step then calls itself quasi-recursively
            self.step()
            return self.results
        except Exception as e:
            # else, dump the current results to file and call an error
            display(e)
            self.results_to_csv('__temp__')
            return self.results
    
    def step(self, i = 0, instructions = None):
        # i is the index of the instruction (within instruction) being called

        # instructions can be provided as a parameter for control flow
        # if not explicitly provided, default to using those in self.instructions
        if instructions is None:
            instructions = self.instructions
        
        # if i == len(instructions), there are no more instructions that need to be called
        if i == len(instructions):
            # so, do nothing
            pass
        else: 
            # by contrast, if i < len(instructions), step will (somewhere) call itself on i + 1

            # if cache is empty, no previous output is available as input for instruction call
            if len(self.cache) == 0:
                # so set self.current to nothing
                self.current = None
            else:
                # else, load up the output of the previous instruction from the end of cache
                self.current = self.cache[-1]
            
            # select the instruction to be run from instructions
            # we use 'layer' here because instruction lists can be nested
            # as such, at this stage, the 'instruction' could be a list
            layer = instructions[i]
            
            if type(layer) is list:
                # if the layer is a list, we step through the list of instructions
                # by calling step - passing in layer as the set of instructions to
                # step through, starting at 0
                self.step(i = 0, instructions = layer)

                # then, we call the instruction after layer, by calling step and passing in
                # our original list of instructions, starting at the index after this one
                self.step(i = i + 1, instructions = instructions)
            else:
                # if the layer is not a list - a single instruction, call the instruction
                # by passing it in to self.call, saving the result as res
                res = self.call(layer)

                # if no result is returned, call an error (even instructions that 
                # only cause side effects - eg. save, assign - return True)

                # and save the current self.data to self.results, then continue
                if res is None:
                    display('Error on instruction {}, returned None.'.format(layer))

                    # save self.data to self.results
                    self.save()
                else:
                    # if the instruction returns either a list or a ResultSet (BS 
                    # equivalent of a list), we need to call the next instruction
                    # on each item of the result
                    if type(res) is list or type(res) is bs4.element.ResultSet:
                        for item in res:
                            # first, append each item to the end of self.cache
                            # this will then be loaded by step into self.current
                            self.cache.append(item)

                            # then call step on the next instruction, passing in
                            # the current list of instructions
                            self.step(i = i + 1, instructions = instructions)

                            # after the next instruction (and the ones after that)
                            # have finished, remove the result of this instruction from 
                            # the end of the cache
                            del self.cache[-1]
                        # as such, we add each item to the cache, call the next 
                        # instruction, then remove it from cache (and repeat)
                    else:
                        # else, if a single value was returned from the instruction
                        # add it to the end of cache, and call step at i + 1
                        # which will call the next instruction in the list 
                        # of instructions provided
                        self.cache.append(res)
                        self.step(i = i + 1, instructions = instructions)
                        # after the next instruction (and the ones after that)
                        # has been run, remove the current output from the end
                        # of the cache
                        del self.cache[-1]
        
    def call(self, instr):
        # used to call an instruction
        # any output from the previous instruction should have been saved in 
        # self.current

        # first, store the instruction in self.current_instr (so we don't have to
        # repeatedly pass it to many different functions)
        self.current_instr = instr

        # depending on instruction name (stored in 'layer'), call the relevant function
        layer = instr['layer']

        if layer == 'scraper':
            return self.scraper()
        elif layer == 'loop':
            return self.loop()
        elif layer == 'splash':
            return self.splash()
        elif layer == 'find':
            return self.find()
        elif layer == 'find_all':
            return self.find_all()
        elif layer == 'text':
            return self.text()
        elif layer == 'value':
            return self.value()
        elif layer == 'assign':
            return self.assign()
        elif layer == 'save':
            return self.save()

    # SECTION 4
    # store instructions / results
        
    def to_csv(self, path = None):
        # saves the current set of instructions to a csv at a provided path
        # if no path is provided, attempts to use self.path
        if path is None and self.path is None:
            # if self.path is empty as well, calls an error
            display('Error - can only save to csv if given a path.')
            return

        if path is None:
            # else, sets path to self.path
            path = self.path

        # loads instructions into a pandas df
        df = pd.DataFrame([ pd.Series(instr) for instr in self.instructions ])

        # uses the pandas inbuilt to_csv method to save as csv at the path provided
        df.to_csv(path)
        
    def from_csv(self, path = None):
        # loads a set of instructions from a csv provided at a provided path
        # if no path is provided, attempts to use self.path
        if path is None and self.path is None:
            # if self.path is empty as well, calls an error
            display('Error - can only get from csv if given a path.')
            return

        if path is None:
            # else, sets path to self.path
            path = self.path

        # loads instructions into a df from csv in path
        df = pd.read_csv(path)

        # converts df to a list of instructions
        self.instructions = df.to_dict('records')
    
    def results_to_csv(self, path = None):
        # saves the current set of results to a csv at a provided path
        # if no path is provided, attempts to use self.results_path
        if path is None and self.results_path is None:
            # if self.results_path is empty as well, calls an error
            display('Error - can only save results to csv if given a path.')
            return

        if path is None:
            # else, sets path to self.path
            path = self.results_path

        # loads instructions into a pandas df
        df = pd.DataFrame([ pd.Series(result) for result in self.results ])

        # uses the pandas inbuilt to_csv method to save as csv at the path provided
        df.to_csv(self.results_path)








