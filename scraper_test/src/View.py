
import pandas as pd
import numpy as np
import ipywidgets as widgets
from IPython.display import display
import qgrid

from src.ducks.Wrap import Wrap
from src.ducks.List import List
from src.ducks.Dict import Dict
from src.ducks.Soup import Soup
from src.ducks.DataFrame import DataFrame

class View(Wrap):

	def __init__(self, app, debug):

		self.app = app

		if debug is True:
			self.debug = {
				'changed': True,
				'handler': True
			}
		else:
			self.debug = {}

		self.header_name = ''
		self.header_from = ''
		self.header_to = ''
		self.header_url = ''

		self.url_clicks = 0
		self.load_clicks = 0

		self.xpath = ''
		self.xpath_clicks = 0

		self.dom_rows = []

		self.dom_expand_all_clicks = 0
		self.dom_contract_all_clicks = 0
		self.dom_expand_one_clicks = 0
		self.dom_contract_one_clicks = 0
		self.dom_expand_one_all_clicks = 0
		self.dom_contract_one_all_clicks = 0

		self.selection_rows = []

		self.selected_type = ''
		self.selected_class = ''
		self.selected_id = ''

		self.custom_single_clicks = 0
		self.custom_many_clicks = 0

		self.select_one_clicks = 0
		self.select_many_clicks = 0
		self.select_text_clicks = 0

		self.scraper_rows = []
		self.scraper_up_clicks = 0
		self.scraper_down_clicks = 0
		self.scraper_delete_clicks = 0

		self.refresh_clicks = 0

		self.output_rows = []
		self.save_clicks = 0

		# Sub components

		self.add_updates = [
			self.update_dom_df_on,
			self.update_selection_df_on,
			self.update_scraper_df_on,
			self.update_output_df_on,
		]

		Wrap.__init__(self)


	# Updates

	def update_dom_df_on(self):
		self.controller.triggers(dom_view_df = self.update_dom_df)

	def update_dom_df(self):
		self.dom_df.df = self.controller.get(dom_view_df = None)


	def update_selection_df_on(self):
		self.controller.triggers(selection_df = self.update_selection_df)

	def update_selection_df(self):
		self.selection_df.df = self.controller.get(selection_df = None)


	def update_scraper_df_on(self):
		self.controller.triggers(scraper_df = self.update_scraper_df)

	def update_scraper_df(self):
		self.crawler_df.df = self.controller.get(scraper_df = None)


	def update_output_df_on(self):
		self.controller.triggers(output_df = self.update_output_df)

	def update_output_df(self):
		self.output_df.df = self.controller.get(output_df = None)

	# Other methods

	def display(self):
		display(self.view)

	def make_view(self):
		self.header = self.make_header()
		self.dom = self.make_dom()
		self.selection = self.make_selection()
		self.crawler = self.make_crawler()
		self.output = self.make_output()

		self.view = widgets.VBox([
			self.header, self.dom, self.selection,
			self.crawler, self.output
		])
		return self.view

	# components

	def new_df(self, df, selection_changed = None):
		grid_default = { "forceFitColumns": False }
		column_default = { 'minWidth': 75, 'maxWidth': 75 }

		widget = qgrid.show_grid(df, show_toolbar = False, \
			grid_options = grid_default, column_options = column_default) 

		if selection_changed is not None:
			widget.on('selection_changed', selection_changed)
		return widget

	def new_input(self, placeholder = None, on_change = None):
		widget = widgets.Text(placeholder = placeholder)
		if on_change is not None:
			widget.observe(on_change, names = 'value')
		return widget

	def new_button(self, name, handler):
		widget = widgets.Button(description = name, disabled = False, \
			tooltip = name, layout = { 'min_width': '150px' } )
		widget.on_click(handler)
		return widget

	def make_header(self):
		name_input = widgets.HBox([self.new_input('name', lambda c: self.assign(header_name = c['new']))])
		from_input = widgets.HBox([
			self.new_input('from_path', lambda c: self.assign(header_from = c['new'])),
			self.new_button('load_from', lambda b: self.assign(load_clicks = self.load_clicks + 1)),
		])
		to_input = widgets.HBox([
			self.new_input('to_path', lambda c: self.assign(header_to = c['new'])),
			self.new_button('save_to', lambda b: self.assign(save_clicks = self.save_clicks + 1)),
		])
		url_input = widgets.HBox([
			self.new_input('url', lambda c: self.assign(header_url = c['new'])),
			self.new_button('get_url', lambda b: self.assign(url_clicks = self.url_clicks + 1)),
		])

		buttons = widgets.VBox([name_input, from_input, to_input, url_input])

		test = widgets.HBox([
			self.new_button('test', lambda b: self.assign(refresh_clicks = self.refresh_clicks + 1))
		])

		return widgets.VBox([buttons, test])

	def make_dom(self):
		button_handlers = {
			'expand_all': lambda b: self.assign(dom_expand_all_clicks = self.dom_expand_all_clicks + 1),
			'contract_all': lambda b: self.assign(dom_contract_all_clicks = self.dom_contract_all_clicks + 1),
			'expand_one': lambda b: self.assign(dom_expand_one_clicks = self.dom_expand_one_clicks + 1),
			'contract_one': lambda b: self.assign(dom_contract_one_clicks = self.dom_contract_one_clicks + 1),
			'expand_one_all': lambda b: self.assign(dom_expand_one_all_clicks = self.dom_expand_one_all_clicks + 1),
			'contract_one_all': lambda b: self.assign(dom_contract_one_all_clicks = self.dom_contract_one_all_clicks + 1),
		}
		buttons = []

		for key, val in button_handlers.items():
			buttons.append(self.new_button(key, val))
		buttons = widgets.HBox(buttons)

		selection_handler = lambda e, w: self.assign(dom_rows = e['new'])

		self.dom_df = self.new_df(self.controller.dom_view_df, selection_handler)

		return widgets.VBox([buttons, self.dom_df])

	def make_selection(self):
		button_handlers = {
			'select_one': lambda b: self.assign(select_one_clicks = self.select_one_clicks + 1),
			'select_all': lambda b: self.assign(select_many_clicks = self.select_many_clicks + 1),
			# 'select_urls': self.parent.select_urls_clicks.incr(),
			# 'select_images': lambda b: self.parent.select_images_clicks.incr(),
		}

		selection_handler = lambda e, w: self.assign(selection_rows = e['new'])

		self.selection_df = self.new_df(self.controller.selection_df, selection_handler)
		self.selection_df.layout.max_width = '300px'


		buttons = []
		for key, val in button_handlers.items():
			buttons.append(self.new_button(key, val))
		buttons = widgets.VBox(buttons)

		type_input = widgets.HBox([
			self.new_input('type', lambda c: self.assign(selected_type = c['new'])),
		])
		id_input = widgets.HBox([
			self.new_input('id', lambda c: self.assign(selected_id = c['new'])),
		])
		class_input = widgets.HBox([
			self.new_input('class', lambda c: self.assign(selected_class = c['new'])),
		])
		select_one = widgets.HBox([
			self.new_button('select_one', lambda b: self.assign(custom_single_clicks = self.custom_single_clicks + 1)),
		])
		select_all = widgets.HBox([
			self.new_button('select_all', lambda b: self.assign(custom_many_clicks = self.custom_many_clicks + 1)),
		])

		inputs = widgets.VBox([type_input, id_input, class_input, select_one, select_all])

		text = self.new_button('select text', lambda b: self.assign(select_text_clicks = self.select_text_clicks + 1))

		block = widgets.HBox([self.selection_df, buttons, inputs, text])

		xpath = widgets.HBox([
			self.new_input('xpath', lambda c: self.assign(xpath = c['new'])),
			self.new_button('load_xpath', lambda b: self.assign(xpath_clicks = self.xpath_clicks + 1))
		])


		return widgets.VBox([xpath, block])

	def make_crawler(self):
		button_handlers = {
			'move_up': lambda b: self.assign(scraper_up_clicks = self.scraper_up_clicks + 1),
			'move_down': lambda b: self.assign(scraper_down_clicks = self.scraper_down_clicks + 1),
			'delete': lambda b: self.assign(scraper_delete_clicks = self.scraper_delete_clicks + 1),
		}

		selection_handler = lambda e, w: self.assign(scraper_rows = e['new'])

		self.crawler_df = self.new_df(self.controller.scraper_df, selection_handler)

		buttons = []
		for key, val in button_handlers.items():
			buttons.append(self.new_button(key, val))
		buttons = widgets.HBox(buttons)

		refresh = widgets.HBox([self.new_button('refresh', lambda b: self.assign(refresh_clicks = self.refresh_clicks + 1))])

		return widgets.VBox([buttons, self.crawler_df, refresh])

	def make_output(self):
		selection_handler = lambda e, w: self.assign(output_rows = e['new'])

		self.output_df = self.new_df(self.controller.output_df, selection_handler)

		refresh = widgets.HBox([self.new_button('save', lambda b: self.assign(save_clicks = self.save_clicks + 1))])

		return widgets.VBox([self.output_df, refresh])
























